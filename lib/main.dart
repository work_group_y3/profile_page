import 'package:flutter/material.dart';

enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade500,
      ),
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade500,
      ),
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.threesixty),
            onPressed: () {
              setState(() {
                currentTheme == APP_THEME.DARK
                    ? currentTheme = APP_THEME.LIGHT
                    : currentTheme = APP_THEME.DARK;
              });
            },
          ),
        ),
      );
  }
}

//PreferredSizeWidget
AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.grey,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.black,
    ),
    actions: <Widget>[
      IconButton(
        onPressed: () {},
        icon: Icon(Icons.star_border),
        color: Colors.black,
      )
    ],
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 250,
            child: Image.network(
              "https://scontent.fbkk10-1.fna.fbcdn.net/v/t39.30808-6/270842958_2076278579216385_1294003299187669043_n.jpg?_nc_cat=108&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=4zvcWNvxiKoAX8L40UQ&_nc_ht=scontent.fbkk10-1.fna&oh=00_AfCkHCPlIpjLEMDiqMG_N6cCUGw0ifNqUuu9O0rx9XGtFA&oe=63A7652D",
              fit: BoxFit.fill,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Kiw Takitchoke Anumon",
                    style: TextStyle(
                      fontSize: 30,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                iconTheme: IconThemeData(
                  color: Colors.deepPurpleAccent,
                ),
              ),
              child: profileActionItems(),
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          mobliePhoneListTile(),
          otherPhoneListTile(),
          Divider(
            color: Colors.grey,
          ),
          emailListTile(),
          addressListTile(),
        ],
      ),
    ],
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

// ListTile
Widget mobliePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text('064-620-4700'),
    subtitle: Text('moblie'),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading: Text(''),
    title: Text('099-990-9999'),
    subtitle: Text('other'),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text('63160005@as.th.com'),
    subtitle: Text('work'),
    trailing: IconButton(
      icon: Text(''),
      // color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text('99 Raya Chburi, Thailand'),
    subtitle: Text('home'),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      // color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}
